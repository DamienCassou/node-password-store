const fs = require("fs")

const gpg = jest.genMockFromModule("gpg")

function decryptFile(filename, callback) {
	fs.readFile(filename, callback)
}

gpg.decryptFile = decryptFile

module.exports = gpg
